
pub const ARCH_X86: u32 = 0;
pub const ARCH_X64: u32 = 1;
pub const ARCH_ARM: u32 = 2;
pub const ARCH_ARM64: u32 = 3;

pub const IS_ARCH_ARM: bool = cfg!(target_arch = "arm");
pub const IS_ARCH_ARM64: bool = cfg!(target_arch = "aarch64");
pub const IS_ARM: bool = IS_ARCH_ARM || IS_ARCH_ARM64;
pub const IS_ARCH_X86: bool = cfg!(target_arch = "x86");
pub const IS_ARCH_X64: bool = cfg!(target_arch = "x86_64");
pub const IS_X86: bool = IS_ARCH_X86 || IS_ARCH_X64;
